export default () => ({
  projectId: null,
  filters: {
    labelName: [],
    milestoneTitle: null,
  },
});
